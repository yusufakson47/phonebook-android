package com.turkcell.challenge.phonebook.ui.contactdetail

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.turkcell.challenge.phonebook.R
import com.turkcell.challenge.phonebook.base.BaseFragment
import com.turkcell.challenge.phonebook.databinding.FragmentEditContactBinding
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.ui.home.AllContactViewModel
import com.turkcell.challenge.phonebook.utils.ADD_CONTACT
import com.turkcell.challenge.phonebook.utils.CONTACT_EDIT
import com.turkcell.challenge.phonebook.utils.getCurrentDate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class EditContactFragment : BaseFragment<FragmentEditContactBinding, AllContactViewModel>() {

    private lateinit var contactModel: ContactModel

    override fun getLayoutRes(): Int {
        return R.layout.fragment_edit_contact
    }

    override fun getViewModelKey(): Class<AllContactViewModel> {
        return AllContactViewModel::class.java
    }

    override fun prepareViews(savedInstanceState: Bundle?) {
        getDataFromArgument()
        setContactInfo()

        binding.editContactButton.setOnClickListener {
            editContactModel()
        }
    }

    private fun getDataFromArgument() {
        val content = arguments?.getParcelable<ContactModel>(CONTACT_EDIT)
        content?.let {
            contactModel = it
        }
    }


    private fun editContactModel() {
        if (checkField()) {
            contactModel =
                ContactModel(
                    id = contactModel.id,
                    createdAt = getCurrentDate(),
                    userAvatar = "",
                    name = binding.nameContactEditText.text.toString(),
                    lastName = binding.lastnameContactEditText.text.toString(),
                    email = binding.emailContactEditText.text.toString(),
                    phoneNumber = binding.phoneContactEditText.text.toString(),
                    companyName = binding.companyContactEditText.text.toString()
                )

            findNavController().navigate(
                R.id.actionEditContactFragmentToContactDetailFragment,
                bundleOf(CONTACT_EDIT to getContact())
            )
        }
    }

    private fun setContactInfo() {
        binding.nameContactEditText.setText(contactModel.name)
        binding.lastnameContactEditText.setText(contactModel.lastName)
        binding.emailContactEditText.setText(contactModel.email)
        binding.phoneContactEditText.setText(contactModel.phoneNumber)
        binding.companyContactEditText.setText(contactModel.companyName)
    }

    private fun getContact(): ContactModel {
        return contactModel
    }

    private fun clearErrorText() {
        binding.nameTextInputLayout.error = ""
        binding.lastNameTextInputLayout.error = ""
        binding.emailTextInputLayout.error = ""
        binding.phoneTextInputLayout.error = ""
        binding.companyContactEditText.error = ""
    }

    private fun checkField(): Boolean {
        clearErrorText()
        var result: Boolean = true

        if (binding.nameContactEditText.text.toString().isEmpty()) {
            binding.nameTextInputLayout.error = getString(R.string.add_contact_error_name)
            result = false
        }

        if (binding.lastnameContactEditText.text.toString().isEmpty()) {
            binding.lastNameTextInputLayout.error = getString(R.string.add_contact_error_lastname)
            result = false
        }

        if (binding.emailContactEditText.text.toString().isEmpty()) {
            binding.emailTextInputLayout.error = getString(R.string.add_contact_error_email)
            result = false
        }

        if (binding.phoneContactEditText.text.toString().isEmpty()) {
            binding.phoneTextInputLayout.error = getString(R.string.add_contact_error_phone)
            result = false
        }

        if (binding.companyContactEditText.text.toString().isEmpty()) {
            binding.companyTextInputLayout.error = getString(R.string.add_contact_error_company)
            result = false
        }

        return result
    }
}