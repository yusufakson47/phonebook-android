package com.turkcell.challenge.phonebook.di


import com.turkcell.challenge.phonebook.network.repository.ContactRepository
import com.turkcell.challenge.phonebook.network.repository.DefaultContactRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@InstallIn(ActivityRetainedComponent::class)
@Module
abstract class RepositoryModule {

    @ActivityRetainedScoped
    @Binds
    abstract fun bindContactRepository(repository: DefaultContactRepository): ContactRepository
}