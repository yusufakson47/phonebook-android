package com.turkcell.challenge.phonebook.ui.splash

import android.content.Intent
import android.os.Handler
import android.os.Looper
import com.turkcell.challenge.phonebook.R
import com.turkcell.challenge.phonebook.base.BaseActivity
import com.turkcell.challenge.phonebook.databinding.ActivitySplashBinding
import com.turkcell.challenge.phonebook.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    private var TIME_OUT: Long = 3000

    override fun getLayoutRes(): Int {
        return R.layout.activity_splash
    }

    override fun initViews() {
        super.initViews()
        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, TIME_OUT)
    }
}