package com.turkcell.challenge.phonebook.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.turkcell.challenge.phonebook.network.model.Status
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.network.repository.ContactRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class AllContactViewModel @Inject constructor(private val contactRepository: ContactRepository) :
    ViewModel() {
    private val _contactsLiveData = MutableLiveData<Status<List<ContactModel>>>()

    val contactsLiveData: LiveData<Status<List<ContactModel>>> = _contactsLiveData

    private val _deletedContactModelLiveData = MutableLiveData<Status<ContactModel>>()

    val deletedContactLiveData: LiveData<Status<ContactModel>> = _deletedContactModelLiveData

    private val _editedContactModelLiveData = MutableLiveData<Status<ContactModel>>()

    val editedContactLiveData: LiveData<Status<ContactModel>> = _editedContactModelLiveData

    private val _addedContactModelLiveData = MutableLiveData<Status<ContactModel>>()

    val addedContactModelLiveData: LiveData<Status<ContactModel>> = _addedContactModelLiveData


    fun getContacts() {
        viewModelScope.launch {
            contactRepository.getAllContacts()
                .onStart { _contactsLiveData.value = Status.loading() }
                .map { response -> Status.fromResponse(response) }
                .collect { status -> _contactsLiveData.value = status }
        }
    }

    fun deleteContact(contactId: String) {
        viewModelScope.launch {
            contactRepository.deleteContact(contactId)
                .onStart { _deletedContactModelLiveData.value = Status.loading() }
                .map { response -> Status.fromResponse(response) }
                .collect { status -> _deletedContactModelLiveData.value = status }
        }
    }

    fun addContact(
        contact: ContactModel
    ) {
        viewModelScope.launch {
            contactRepository.addContact(
                contact.id,
                contact.createdAt,
                contact.userAvatar,
                contact.companyName,
                contact.email,
                contact.name,
                contact.lastName,
                contact.phoneNumber,
            )
                .onStart { _addedContactModelLiveData.value = Status.loading() }
                .map { response -> Status.fromResponse(response) }
                .collect { status -> _addedContactModelLiveData.value = status }
        }
    }


    fun editContact(
        contact: ContactModel
    ) {
        viewModelScope.launch {
            contactRepository.editContact(
                contact.id,
                contact.createdAt,
                contact.userAvatar,
                contact.companyName,
                contact.email,
                contact.name,
                contact.lastName,
                contact.phoneNumber,
            )
                .onStart { _editedContactModelLiveData.value = Status.loading() }
                .map { response -> Status.fromResponse(response) }
                .collect { status -> _editedContactModelLiveData.value = status }
        }
    }
}