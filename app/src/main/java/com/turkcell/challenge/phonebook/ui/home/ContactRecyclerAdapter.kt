package com.turkcell.challenge.phonebook.ui.home

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.turkcell.challenge.phonebook.databinding.RowContactRecyclerBinding
import com.turkcell.challenge.phonebook.network.model.ContactModel
import java.util.*

class ContactRecyclerAdapter(
    private val onItemClicked: (ContactModel) -> Unit
) : ListAdapter<ContactModel, ContactRecyclerAdapter.ContactsViewHolder>(DIFF_CALLBACK),
    Filterable {

    private var contactList = mutableListOf<ContactModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ContactsViewHolder(
        RowContactRecyclerBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) =
        holder.bind(getItem(position), onItemClicked)

    fun setData(contactList: MutableList<ContactModel>) {
        this.contactList = contactList
        submitList(contactList)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ContactModel>() {
            override fun areItemsTheSame(oldItem: ContactModel, newItem: ContactModel): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: ContactModel, newItem: ContactModel): Boolean =
                oldItem == newItem
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {

                val filteredList = mutableListOf<ContactModel>()
                val charSearch = constraint.toString()

                if (constraint == null || constraint.isEmpty()) {
                    filteredList.addAll(contactList)
                } else {
                    for (item in contactList) {
                        if (item.name.lowercase(Locale.ROOT).contains(
                                charSearch.lowercase(Locale.ROOT)
                            ) ||
                            item.lastName.lowercase(Locale.ROOT)
                                .contains(charSearch.lowercase(Locale.ROOT))
                        ) {
                            filteredList.add(item)
                        }
                    }
                }
                val results = FilterResults()
                results.values = filteredList
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                submitList(results?.values as MutableList<ContactModel>)
            }
        }
    }

    class ContactsViewHolder(private val binding: RowContactRecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(contact: ContactModel, onItemClicked: (ContactModel) -> Unit) {
            binding.personNameAndSurnameTextView.text = contact.name + " " + contact.lastName
            binding.personPhoneNumberTextView.text = contact.phoneNumber

            binding.root.setOnClickListener {
                onItemClicked(contact)
            }
        }
    }
}