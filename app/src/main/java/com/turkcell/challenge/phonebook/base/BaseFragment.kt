package com.turkcell.challenge.phonebook.base

import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.turkcell.challenge.phonebook.utils.autoCleared

abstract class BaseFragment <VB : ViewDataBinding, VM : ViewModel> : Fragment() {

    /**
     * Returns Current ViewModel Instance
     */
    val viewModel: VM by lazy { ViewModelProvider(this).get(getViewModelKey()) }
    var binding by autoCleared<VB>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize UI
        prepareViews(savedInstanceState)
    }

    /**
     * Layout resource id
     */
    @LayoutRes
    protected abstract fun getLayoutRes(): Int


    /**
     * Returns ViewModel class type
     */
    protected abstract fun getViewModelKey(): Class<VM>


    /**
     * Prepare UI Components here
     */
    abstract fun prepareViews(savedInstanceState: Bundle?)

}