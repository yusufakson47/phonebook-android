package com.turkcell.challenge.phonebook.network.repository

import androidx.annotation.MainThread
import com.turkcell.challenge.phonebook.network.dao.ContactDao
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.network.model.GenericResponse
import com.turkcell.challenge.phonebook.network.ApiService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import retrofit2.Response
import javax.inject.Inject

@ExperimentalCoroutinesApi
class DefaultContactRepository @Inject constructor(
    private val contactDao: ContactDao,
    private val apiService: ApiService
) : ContactRepository {

    /**
     * Fetched the data from network and stored it in database. At the end, data from persistence
     * storage is fetched and emitted.
     */
    override fun getAllContacts(): Flow<GenericResponse<List<ContactModel>>> {
        return object : NetworkBoundRepository<List<ContactModel>, List<ContactModel>>() {

            override suspend fun saveRemoteData(response: List<ContactModel>) =
                contactDao.addContacts(response)

            override fun fetchFromLocal(): Flow<List<ContactModel>> = contactDao.getAllContacts()

            override suspend fun fetchFromRemote(): Response<List<ContactModel>> =
                apiService.getContacts()
        }.asFlow()
    }

    @MainThread
    override fun getContactById(id: String): Flow<ContactModel> =
        contactDao.getContactById(id).distinctUntilChanged()

    override fun deleteContact(id: String): Flow<GenericResponse<ContactModel>> {
        return object : NetworkBoundRepository<ContactModel, ContactModel>() {
            override suspend fun saveRemoteData(response: ContactModel) {
                contactDao.deleteContactById(id)
            }

            override fun fetchFromLocal(): Flow<ContactModel> = contactDao.getContactById(id)

            override suspend fun fetchFromRemote(): Response<ContactModel> {
                contactDao.deleteContactById(id)
                return apiService.deleteContact(id)
            }

        }.asFlow()
    }

    override fun addContact(
         id: String,
         createdAt: String,
         userAvatar: String,
         companyName: String,
         email: String,
         name: String,
         lastName: String,
         phoneNumber: String
    ): Flow<GenericResponse<ContactModel>> {
        return object : NetworkBoundRepository<ContactModel, ContactModel>() {
            override suspend fun saveRemoteData(response: ContactModel) {
                contactDao.addContact(response)
            }

            override fun fetchFromLocal(): Flow<ContactModel> = contactDao.getContactById(id)

            override suspend fun fetchFromRemote(): Response<ContactModel> =
                apiService.addContact(
                    ContactModel(
                        id = id,
                        createdAt = createdAt,
                        companyName = companyName,
                        userAvatar = userAvatar,
                        email = email,
                        name = name,
                        lastName = lastName,
                        phoneNumber = phoneNumber
                    )
                )

        }.asFlow()
    }

    override fun editContact(
        id: String,
        createdAt: String,
        userAvatar: String,
        companyName: String,
        email: String,
        name: String,
        lastName: String,
        phoneNumber: String
    ): Flow<GenericResponse<ContactModel>> {
        return object : NetworkBoundRepository<ContactModel, ContactModel>() {
            override suspend fun saveRemoteData(response: ContactModel) {
                contactDao.editContact(
                    id = id,
                    createdAt = createdAt,
                    companyName = companyName,
                    email = email,
                    name = name,
                    lastName = lastName,
                    phoneNumber = phoneNumber
                )
            }

            override fun fetchFromLocal(): Flow<ContactModel> = contactDao.getContactById(id)

            override suspend fun fetchFromRemote(): Response<ContactModel> {
                return apiService.editContact(
                    id = id,
                    ContactModel(
                        id = id,
                        createdAt = createdAt,
                        companyName = companyName,
                        userAvatar = userAvatar,
                        email = email,
                        name = name,
                        lastName = lastName,
                        phoneNumber = phoneNumber
                    )
                )
            }
        }.asFlow()
    }
}