package com.turkcell.challenge.phonebook.network.repository

import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.network.model.GenericResponse
import kotlinx.coroutines.flow.Flow

interface ContactRepository {
    fun getAllContacts(): Flow<GenericResponse<List<ContactModel>>>
    fun getContactById(id: String): Flow<ContactModel>
    fun deleteContact(id: String): Flow<GenericResponse<ContactModel>>
    fun addContact(
         id: String,
         createdAt: String,
         userAvatar: String,
         companyName: String,
         email: String,
         name: String,
         lastName: String,
         phoneNumber: String
    ): Flow<GenericResponse<ContactModel>>

    fun editContact(
        id: String,
         createdAt: String,
         userAvatar: String,
         companyName: String,
         email: String,
         name: String,
         lastName: String,
         phoneNumber: String
    ): Flow<GenericResponse<ContactModel>>
}