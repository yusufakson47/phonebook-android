package com.turkcell.challenge.phonebook.utils.customview

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton

class ButtonBold(context: Context?, attrs: AttributeSet?) :
    AppCompatButton(context!!, attrs) {

    var mContext: Context? = null
    private fun init() {
        try {
            val font = Typeface.createFromAsset(context!!.assets, "font/SanFranciscoText-Bold.otf")
            typeface = font
        } catch (e: Exception) {
        }
    }

    override fun setTypeface(tf: Typeface?) {
        super.setTypeface(tf)
    }

    init {
        this.mContext = context
        init()
    }
}