package com.turkcell.challenge.phonebook.network.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.turkcell.challenge.phonebook.network.model.GenericResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import retrofit2.Response

/**
 * A repository which provides resource from local database as well as remote end point.
 *
 * [RESULT] represents the type for database.
 * [REQUEST] represents the type for network.
 */
@ExperimentalCoroutinesApi
abstract class NetworkBoundRepository<RESULT, REQUEST> {

    fun asFlow() = flow<GenericResponse<RESULT>> {

        // Emit Database content first
        emit(GenericResponse.Success(fetchFromLocal().first()))

        // Fetch latest data from remote
        val apiResponse = fetchFromRemote()

        // Parse body
        val remoteData = apiResponse.body()

        // Check for response validation
        if (apiResponse.isSuccessful && remoteData != null) {
            // Save data into the persistence storage
            saveRemoteData(remoteData)
        } else {
            // Something went wrong! Emit Error state.
            emit(GenericResponse.Failed(apiResponse.message()))
        }

        // Retrieve data from persistence storage and emit
        emitAll(
            fetchFromLocal().map {
                GenericResponse.Success<RESULT>(it)
            }
        )
    }.catch { e ->
        e.printStackTrace()
        emit(GenericResponse.Failed("Network error! Please try again."))
    }

    /**
     * Saves retrieved data from remote into the persistence storage.
     */
    @WorkerThread
    protected abstract suspend fun saveRemoteData(response: REQUEST)

    /**
     * Retrieves all data from persistence storage.
     */
    @MainThread
    protected abstract fun fetchFromLocal(): Flow<RESULT>

    /**
     * Fetches [GenericResponse] from the remote end point.
     */
    @MainThread
    protected abstract suspend fun fetchFromRemote(): Response<REQUEST>
}