package com.turkcell.challenge.phonebook.utils

//Room Database
const val CONTACTS_TABLE = "contacts_table"
const val DB_NAME = "db_contacts"

//Contact Intent object
const val CONTACT_DETAILS = "CONTACT_DETAILS"
const val CONTACT_DELETED = "CONTACT_DELETED"
const val CONTACT_EDIT = "CONTACT_EDIT"
const val ADD_CONTACT = "ADD_CONTACT"
