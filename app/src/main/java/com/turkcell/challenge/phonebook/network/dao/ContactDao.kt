package com.turkcell.challenge.phonebook.network.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.utils.CONTACTS_TABLE
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactDao {
    /**
     * Inserts [contacts] into the [Contact CONTACTS_TABLE] table.
     * Duplicate values are replaced in the table.
     * @param contacts
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addContacts(contacts: List<ContactModel>)

    /**
     * Inserts [contact] into the [Contact CONTACTS_TABLE] table.
     * Duplicate values are replaced in the table.
     * @param contact
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addContact(contact: ContactModel)

    /**
     * Deletes where [ContactModel.id] = [id] from the [[CONTACTS_TABLE] table.
     */
    @Query("DELETE FROM ${CONTACTS_TABLE} WHERE id = :id")
    fun deleteContactById(id: String)

    /**
     * Updates where [ContactModel.id] = [id] from the [CONTACTS_TABLE] table.
     * @param createdAt
     * @param companyName
     * @param email
     * @param name
     * @param lastName
     * @param phoneNumber
     */
    @Query("UPDATE ${CONTACTS_TABLE} set createdAt = :createdAt, companyName = :companyName, email = :email, name = :name,lastName = :lastName, phoneNumber = :phoneNumber WHERE id = :id")
    fun editContact(
        id: String,
        createdAt: String,
        companyName: String,
        email: String,
        name: String,
        lastName: String,
        phoneNumber: String
    )

    /**
     * Fetches the data from the [CONTACTS_TABLE] table whose value is [contactId].
     * @param id is unique ID of [ContactModel]
     * @return [Flow] of [ContactModel] from database table.
     */
    @Query("SELECT * FROM ${CONTACTS_TABLE} WHERE id = :id")
    fun getContactById(id: String): Flow<ContactModel>

    /**
     * Fetches all the data from the [CONTACTS_TABLE] table.
     * @return [Flow]
     */
    @Query("SELECT * FROM ${CONTACTS_TABLE}")
    fun getAllContacts(): Flow<List<ContactModel>>
}