package com.turkcell.challenge.phonebook.network.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.turkcell.challenge.phonebook.utils.CONTACTS_TABLE
import kotlinx.parcelize.Parcelize


@Parcelize
@Entity(tableName = CONTACTS_TABLE)
data class ContactModel(
    @PrimaryKey
    @NonNull
    val id: String,
    val createdAt: String,
    val userAvatar: String,
    val companyName: String,
    val email: String,
    val name: String,
    val lastName: String,
    val phoneNumber: String
) : Parcelable