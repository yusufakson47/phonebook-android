package com.turkcell.challenge.phonebook.ui.home

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputLayout
import com.turkcell.challenge.phonebook.R
import com.turkcell.challenge.phonebook.base.BaseFragment
import com.turkcell.challenge.phonebook.databinding.FragmentAddNewContactBinding
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.utils.ADD_CONTACT
import com.turkcell.challenge.phonebook.utils.getCurrentDate
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AddNewContactFragment : BaseFragment<FragmentAddNewContactBinding, AllContactViewModel>() {

    private lateinit var contactModel: ContactModel

    override fun getViewModelKey(): Class<AllContactViewModel> {
        return AllContactViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_add_new_contact
    }


    override fun prepareViews(savedInstanceState: Bundle?) {
        binding.addContactButton.setOnClickListener {
            setContactModel()
        }
    }

    private fun setContactModel() {
        if (checkField()) {
            contactModel =
                ContactModel(
                    id = "",
                    createdAt = getCurrentDate(),
                    userAvatar = "",
                    name = binding.nameContactEditText.text.toString(),
                    lastName = binding.lastnameContactEditText.text.toString(),
                    email = binding.emailContactEditText.text.toString(),
                    phoneNumber = binding.phoneContactEditText.text.toString(),
                    companyName = binding.companyContactEditText.text.toString()
                )

            findNavController().navigate(
                R.id.actionAllContactFragmentToAddNewContactFragment,
                bundleOf(ADD_CONTACT to getContact())
            )
        }
    }

    private fun getContact(): ContactModel {
        return contactModel
    }

    private fun clearErrorText() {
        binding.nameTextInputLayout.error = ""
        binding.lastNameTextInputLayout.error = ""
        binding.emailTextInputLayout.error = ""
        binding.phoneTextInputLayout.error = ""
        binding.companyContactEditText.error = ""
    }

    private fun checkField(): Boolean {
        clearErrorText()
        var result: Boolean = true

        if (binding.nameContactEditText.text.toString().isEmpty()) {
            binding.nameTextInputLayout.error = getString(R.string.add_contact_error_name)
            result = false
        }

        if (binding.lastnameContactEditText.text.toString().isEmpty()) {
            binding.lastNameTextInputLayout.error = getString(R.string.add_contact_error_lastname)
            result = false
        }

        if (binding.emailContactEditText.text.toString().isEmpty()) {
            binding.emailTextInputLayout.error = getString(R.string.add_contact_error_email)
            result = false
        }

        if (binding.phoneContactEditText.text.toString().isEmpty()) {
            binding.phoneTextInputLayout.error = getString(R.string.add_contact_error_phone)
            result = false
        }

        if (binding.companyContactEditText.text.toString().isEmpty()) {
            binding.companyTextInputLayout.error = getString(R.string.add_contact_error_company)
            result = false
        }

        return result
    }
}