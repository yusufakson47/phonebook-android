package com.turkcell.challenge.phonebook.network.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.utils.DB_NAME

@Database(
    entities = [ContactModel::class],
    version = DBMigration.DB_VERSION
)
abstract class DBContact : RoomDatabase() {

    abstract fun getContactDao(): ContactDao

    companion object {

        @Volatile
        private var INSTANCE: DBContact? = null

        fun getInstance(context: Context): DBContact {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DBContact::class.java,
                    DB_NAME
                ).addMigrations(*DBMigration.MIGRATION_CONTACT)
                    .allowMainThreadQueries()
                    .build()

                INSTANCE = instance
                return instance
            }
        }
    }
}
