package com.turkcell.challenge.phonebook

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PhonebookApp :Application()