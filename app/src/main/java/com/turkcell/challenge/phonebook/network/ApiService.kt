package com.turkcell.challenge.phonebook.network

import com.turkcell.challenge.phonebook.network.model.ContactModel
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("contactsData")
    suspend fun getContacts(): Response<List<ContactModel>>

    @POST("contactsData")
    suspend fun addContact(@Body contact: ContactModel): Response<ContactModel>

    @DELETE("contactsData/{id}")
    suspend fun deleteContact(@Path("id") id: String): Response<ContactModel>

    @PUT("contactsData/{id}")
    suspend fun editContact(@Path("id") id: String, @Body contact: ContactModel): Response<ContactModel>

}