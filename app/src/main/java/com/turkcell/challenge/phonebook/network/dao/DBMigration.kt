package com.turkcell.challenge.phonebook.network.dao

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.turkcell.challenge.phonebook.utils.CONTACTS_TABLE

object DBMigration {
    const val DB_VERSION = 1

    val MIGRATION_CONTACT: Array<Migration>
        get() = arrayOf(
            migrationContact()
        )

    private fun migrationContact(): Migration = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE ${CONTACTS_TABLE} ADD COLUMN body TEXT")
        }
    }
}