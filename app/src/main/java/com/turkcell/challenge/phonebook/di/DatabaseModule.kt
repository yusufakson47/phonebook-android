package com.turkcell.challenge.phonebook.di

import android.app.Application
import com.turkcell.challenge.phonebook.network.dao.DBContact
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDBContact(application: Application) = DBContact.getInstance(application)

    @Singleton
    @Provides
    fun provideContactDao(database: DBContact) = database.getContactDao()
}