package com.turkcell.challenge.phonebook.utils.customview

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class TextViewMedium constructor(context: Context?, attrs: AttributeSet?) :
    AppCompatTextView(context!!, attrs) {

    private var myContext: Context? = null
    private fun init() {
        try {
            val font = Typeface.createFromAsset(context!!.assets, "font/SanFranciscoText-Medium.otf")
            typeface = font
        } catch (e: Exception) {
        }
    }

    override fun setTypeface(tf: Typeface?) {
        super.setTypeface(tf)
    }

    init {
        this.myContext = context
        init()
    }
}