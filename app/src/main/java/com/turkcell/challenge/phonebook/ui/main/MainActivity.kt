package com.turkcell.challenge.phonebook.ui.main

import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.turkcell.challenge.phonebook.R
import com.turkcell.challenge.phonebook.base.BaseActivity
import com.turkcell.challenge.phonebook.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    override fun getLayoutRes(): Int {
        return R.layout.activity_main
    }

    override fun onSupportNavigateUp() =
        getNavController().navigateUp() || super.onSupportNavigateUp()

    private fun getNavController(): NavController {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        return navHostFragment.navController
    }
}