package com.turkcell.challenge.phonebook.ui.contactdetail

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.turkcell.challenge.phonebook.network.model.Status
import com.turkcell.challenge.phonebook.R
import com.turkcell.challenge.phonebook.base.BaseFragment
import com.turkcell.challenge.phonebook.databinding.FragmentDetailContactBinding
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.ui.home.AllContactViewModel
import com.turkcell.challenge.phonebook.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ContactDetailFragment : BaseFragment<FragmentDetailContactBinding, AllContactViewModel>() {

    private lateinit var contactModel: ContactModel

    override fun getLayoutRes(): Int {
        return R.layout.fragment_detail_contact
    }

    override fun getViewModelKey(): Class<AllContactViewModel> {
        return AllContactViewModel::class.java
    }

    override fun onStart() {
        super.onStart()
        initObserverDeleteContact()
    }

    override fun prepareViews(savedInstanceState: Bundle?) {
        getDataFromArgument()
        initView()
    }

    private fun getDataFromArgument() {
        val contentDetails = arguments?.getParcelable<ContactModel>(CONTACT_DETAILS)
        val contentEdit = arguments?.getParcelable<ContactModel>(CONTACT_EDIT)

        contentDetails?.let {
            contactModel = it
        }

        contentEdit?.let { isFromEditContact ->
            contactModel = isFromEditContact
            showToast(getString(R.string.contact_info_edit_success_message))
            editContact(contactModel)
            arguments?.remove(CONTACT_EDIT)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        binding.run {
            contactNameAndSurname.text = contactModel.name + " " + contactModel.lastName
            contactDetailPhoneTextView.text = contactModel.phoneNumber
            contactDetailEmailTextView.text = contactModel.email
            contactDetailCompanyTextView.text = contactModel.companyName
        }

        binding.editContactImageView.setOnClickListener {
            findNavController().navigate(
                R.id.actionContactDetailFragmentToEditContactFragment,
                bundleOf(CONTACT_EDIT to contactModel)
            )
        }

        binding.deleteContactImageView.setOnClickListener {
            alertDeleteContact()
        }
    }

    private fun alertDeleteContact() {
        requireActivity().showAlertDialog {
            setMessage(getString(R.string.person_delete_dialog_message))
            positiveButton(text = getString(R.string.ok)) {
                deleteContact(contactModel.id)
            }
            negativeButton(text = getString(R.string.cancel))
        }
    }

    private fun deleteContact(id: String) = viewModel.deleteContact(id)

    private fun initObserverDeleteContact() {
        viewModel.deletedContactLiveData.observe(viewLifecycleOwner) { state ->
            when (state) {
                is Status.Loading -> {
                    showToast(requireActivity().getString(R.string.please_wait))
                }

                is Status.Success -> {
                    findNavController().navigate(
                        R.id.actionContactDetailFragmentToContactFragment,
                        bundleOf(CONTACT_DELETED to true)
                    )
                }
                is Status.Error -> {
                    showToast(state.message)
                }
            }
        }
    }

    private fun editContact(contact: ContactModel) = viewModel.editContact(contact)

}