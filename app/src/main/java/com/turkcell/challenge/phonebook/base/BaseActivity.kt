package com.turkcell.challenge.phonebook.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<VB: ViewDataBinding>: AppCompatActivity() {

    lateinit var binding: VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())
        initViews()
    }

    /**
     * Layout resource id
     */
    @LayoutRes
    protected abstract fun getLayoutRes(): Int

    open fun initViews() {}

}