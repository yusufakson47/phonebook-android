package com.turkcell.challenge.phonebook.ui.home

import android.os.Bundle
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.turkcell.challenge.phonebook.network.model.Status
import com.turkcell.challenge.phonebook.R
import com.turkcell.challenge.phonebook.base.BaseFragment
import com.turkcell.challenge.phonebook.databinding.FragmentAllContactsBinding
import com.turkcell.challenge.phonebook.network.model.ContactModel
import com.turkcell.challenge.phonebook.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AllContactFragment : BaseFragment<FragmentAllContactsBinding, AllContactViewModel>() {

    private val contactRecyclerAdapter = ContactRecyclerAdapter(this::onItemClicked)
    private lateinit var contactModel: ContactModel

    override fun getLayoutRes(): Int {
        return R.layout.fragment_all_contacts
    }

    override fun getViewModelKey(): Class<AllContactViewModel> {
        return AllContactViewModel::class.java
    }

    override fun prepareViews(savedInstanceState: Bundle?) {
        initView()
        searchContact()
        deletedContact()
        addContact()
    }

    override fun onStart() {
        super.onStart()
        observeContacts()
    }

    override fun onResume() {
        super.onResume()
        getContacts()
    }

    private fun getContacts() = viewModel.getContacts()

    private fun initView() {
        binding.run {
            binding.contactRecyclerView.adapter = contactRecyclerAdapter
            binding.contactRecyclerView.setHasFixedSize(true)
            binding.swipeRefreshAllContact.setOnRefreshListener {
                getContacts()
            }
        }

        binding.newPostActionButton.setOnClickListener {
            findNavController().navigate(R.id.actionAllContactFragmentToAddNewContactFragment)
        }

        viewModel.contactsLiveData.value?.let { currentState ->
            if (!currentState.isSuccessful()) {
                getContacts()
            }
        }

    }

    private fun observeContacts() {
        viewModel.contactsLiveData.observe(viewLifecycleOwner) { state ->
            when (state) {
                is Status.Loading -> showLoading(true)
                is Status.Success -> {
                    if (state.data.isNotEmpty()) {
                        contactRecyclerAdapter.setData(state.data.toMutableList())
                    }
                    showLoading(false)
                }
                is Status.Error -> {
                    showToast(state.message)
                    showLoading(false)
                }
            }
        }
    }


    private fun addContact() {
        val content = arguments?.getParcelable<ContactModel>(ADD_CONTACT)
        content?.let { isAddedNewContact ->
            contactModel = isAddedNewContact
            addContact(contactModel)
            showToast(getString(R.string.contact_added_message))
            getContacts()
            arguments?.remove(ADD_CONTACT)
        }
    }

    private fun addContact(contact: ContactModel) = viewModel.addContact(contact)


    private fun deletedContact() {
        val content = arguments?.getBoolean(CONTACT_DELETED)
        content?.let { isDeleted ->
            if (isDeleted) {
                showToast(getString(R.string.person_deleted_message))
                getContacts()
                arguments?.remove(CONTACT_DELETED)
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.swipeRefreshAllContact.isRefreshing = isLoading
    }

    private fun onItemClicked(contact: ContactModel) {
        findNavController().navigate(
            R.id.actionAllContactFragmentToContactDetailFragment,
            bundleOf(CONTACT_DETAILS to contact)
        )
    }

    private fun searchContact() =
        binding.contactSearchView.setOnQueryTextListener(filterContacts())

    private fun filterContacts() = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            contactRecyclerAdapter.filter.filter(newText)
            return false
        }
    }

}